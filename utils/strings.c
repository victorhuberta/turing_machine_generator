/**
 * Module: generator
 * Author: Victor Huberta
 * ```
 * Provide all strings-related manipulation functions.
 */

#include "strings.h"

/**
 * Written by: http://stackoverflow.com/users/1033896/hmjd
 * note: It does not handle consecutive delimiters, e.g., "JAN,,,FEB,MAR".
 */
char** str_split(char* str, const char a_delim)
{
    char *a_str, **result;
    size_t count = 0;
    char *tmp, *last_comma;
    char delim[2];

    delim[0] = a_delim;
    delim[1] = 0;

    /* Make a copy of the input string */
    a_str = strdup(str);
    if (a_str == NULL) return NULL;

    tmp = a_str;

    /* Count how many elements will be extracted. */
    while (*tmp) {
        if (a_delim == *tmp) {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = calloc(count, sizeof(char*));

    if (result != NULL) {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token) {
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        *(result + idx) = 0;
    }

    free(a_str);
    return result;
}

void free_strings(char **strings)
{
    int s;

    for (s = 0; strings[s] != 0; ++s)
        free(strings[s]);
    free(strings);
}
