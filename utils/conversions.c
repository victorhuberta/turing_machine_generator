/**
 * Module: conversions
 * Author: Victor Huberta
 * ```
 * Provide all conversions-related functions.
 */

#include "conversions.h"

int char_to_int(char c)
{
    char str[2];

    str[0] = c;
    str[1] = '\0';

    return atoi(str);
}
