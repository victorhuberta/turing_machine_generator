#ifndef STRINGS_H
#define STRINGS_H

#include "../main.h"

char** str_split(char* str, const char a_delim);
void free_strings(char **strings);

#endif
