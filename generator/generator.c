/**
 * Module: generator
 * Author: Victor Huberta
 * ```
 * Given a sample input, a set of all possible input alphabets, and an
 * expected output, generate a working Turing machine. Genetic algorithm
 * is used to increase the chance of generating a correct machine.
 */

#include "generator.h"

static char *alphabets;
static char *input;
static char *output;

void init_generator(char *_alphabets, char *_input, char *_output)
{
    int n_alphabets;

    alphabets = _alphabets;
    for (n_alphabets = 0; alphabets[n_alphabets] != '\0'; ++n_alphabets);
    input = _input;
    output = _output;

    init_population(alphabets, n_alphabets);
    init_evolution(alphabets, n_alphabets);
}

/**
 * Gathers a random population from all possible genes. Test the
 * fitness of every chromosome in the population until it finds
 * a chromosome with perfect fitness. If not found, it will evolve
 * the current population into a new population with better genes,
 * and then test its chromosomes' fitness again.
 */
void generate_machine(void)
{
    int p;
    Chromosome **population;
    long n_generations = 1;
    double total_fitness = 0.0;

    population = get_random_population();

    while (1) {
        if ((p = test_solutions(population, &total_fitness)) >= 0)
            _print_correct_machine(population[p]->spec, n_generations);

        printf("\nTotal fitness = %f & generation no. = %ld\n\n",
            total_fitness, n_generations);
        population = evolve(total_fitness, population);

        if (n_generations >= N_ALLOWED_GENERATIONS)
            _print_generate_failure(n_generations);

        ++n_generations;
    }
}

/**
 * Get the fitness of every chromosome and add it to the total fitness.
 * At the same time, find the perfect chromosome.
 */
int test_solutions(Chromosome **population, double *total_fitness)
{
    int p;
    Chromosome *chromosome;

    for (p = 0; p < N_POPULATION; ++p) {
        chromosome = population[p];

        printf("Testing: %.*s...\n", 80, chromosome->spec);

        chromosome->fitness = get_fitness(chromosome, input, output);
        *total_fitness += chromosome->fitness;

        if (chromosome->fitness == N_FITTEST) return p;
    }

    return -1;
}

void _print_correct_machine(char *spec, long n_generations)
{
    printf("Correct machine created after %ld generation(s)!\n",
        n_generations);
    printf("%s\n", spec);
    exit(EXIT_SUCCESS);
}

void _print_generate_failure(long n_generations)
{
    printf("No correct machine created, after %ld generation(s)\n",
        n_generations);
    exit(EXIT_SUCCESS);
}
