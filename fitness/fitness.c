/**
 * Module: fitness
 * Author: Victor Huberta
 * ```
 * Evaluate the fitness of a chromosome by testing the solution
 * and then get the fitness score based on a fitness function.
 */

#include "fitness.h"

/**
 * Run the generated machine and check its output's edit distance
 * from the expected output. A perfect score is 999.0.
 */
double get_fitness(Chromosome *chromosome, char *input, char *expected)
{
    Machine *machine;
    char *output;
    int edit_distance;

    machine = create_machine(chromosome->spec);
    output = run_machine(machine, input);
    edit_distance = levenshtein(expected, output);

    free(output);
    free_machine(machine);

    return (edit_distance == 0) ? 999.0 : 1 / edit_distance;
}

/**
 * Taken from:
 * https://en.wikibooks.org/wiki/Algorithm_Implementation/
 * Strings/Levenshtein_distance#C
 */

#define MIN3(a, b, c) ((a) < (b) ? ((a) < (c) ? \
(a) : (c)) : ((b) < (c) ? (b) : (c)))

int levenshtein(char *s1, char *s2)
{
    unsigned int s1len, s2len, x, y, lastdiag, olddiag;
    unsigned int *column;
    int result;

    s1len = strlen(s1);
    s2len = strlen(s2);

    column = calloc(s1len + 1, sizeof *column);

    for (y = 1; y <= s1len; y++)
        column[y] = y;
    for (x = 1; x <= s2len; x++) {
        column[0] = x;
        for (y = 1, lastdiag = x-1; y <= s1len; y++) {
            olddiag = column[y];
            column[y] = MIN3(column[y] + 1, column[y-1] + 1,
                lastdiag + (s1[y-1] == s2[x-1] ? 0 : 1));
            lastdiag = olddiag;
        }
    }

    result = column[s1len];

    free(column);
    return(result);
}
