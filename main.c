/**
 * Project: Turing Machine Generator
 * Author: Victor Huberta
 * ```
 * Read a set of input alphabets, an input string,
 * and the expected output string from user.
 * Generate a working Turing machine which manipulates the
 * input string to produce the expected output string.
 *
 * For more details, read the README of the project.
 */

#include "main.h"
#include "interpreter/interpreter.h"
#include "generator/generator.h"

int main(int argc, char **argv)
{
    char *alphabets, *input, *output;

    if (argc != 4) {
        printf("Usage: ./%s alphabets input output\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    alphabets = argv[1];
    input = argv[2];
    output = argv[3];

    init_generator(alphabets, input, output);
    generate_machine();

    exit(EXIT_SUCCESS);
}
