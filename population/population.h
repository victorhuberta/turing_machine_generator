#ifndef POPULATION_H
#define POPULATION_H

#include "../main.h"
#include "../utils/errors.h"
#include "../interpreter/interpreter.h"

#define N_POPULATION 100 /* Must be an even number. */
#define N_SPEC_LEN 300
#define N_MAX_STATES 8
#define N_GENE_CHARS 4 /* r, w, shift, and next state */
#define N_SHIFT_TYPES 2 /* L and R */

typedef struct Chromosome {
    char *spec;
    double fitness;
    int n_states;
} Chromosome;

typedef struct GeneType {
    char name, **genes;
    int n_genes;
} GeneType;

void init_population(char *_alphabets, int _n_alphabets);
Chromosome **get_random_population(void);
Chromosome *new_chromosome(char *spec, double fitness, int n_states);
Chromosome *write_spec_random(GeneType **gene_types,
    Chromosome *chromosome, int n_states);
GeneType **get_all_genes(int n_states);
GeneType *new_genetype(char alphabet, char **genes, int n_genes);
char **new_genes(char alphabet, int n_genes, int n_states);
void free_gene_types(GeneType **gene_types);
void free_population(Chromosome **population);

#endif
