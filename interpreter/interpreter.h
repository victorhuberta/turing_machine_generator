#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "../main.h"
#include "../utils/errors.h"
#include "../utils/strings.h"
#include "../utils/conversions.h"

typedef struct Operation {
    char r, w, shift;
    int next_state;
} Operation;

typedef struct State {
    Operation **ops;
    int n_ops;
} State;

typedef struct Machine {
    State **states;
    int n_states;
} Machine;

#define ST_DELIM ';'
#define OP_DELIM ','

#define READ 0
#define WRITE 1
#define SHIFT 2
#define NEXT_ST 3
#define LEFT 'L'
#define RIGHT 'R'

Machine *create_machine(char *spec);
Machine *new_machine(State **states, int n_states);
State *new_state(Operation **ops, int n_ops);
Operation *new_operation(char r, char w, char shift, int next_state);


#define OUTPUT_LEN 100
#define N_ALLOWED_OPERATIONS 100
char *run_machine(Machine *machine, char *input);
void free_machine(Machine *machine);

void test_interpreter(void);

#endif
